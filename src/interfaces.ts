export interface User {
  name: string
  id: number
  password: string
}

export interface ISession {
  userId: number
  userPassword: string
  counter: number
  expires: number
}
