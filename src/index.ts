import * as Koa from 'koa'
// TODO: make session object interface
// TODO: use session middleware in koa app on high leve

import { ISession } from './interfaces'
import { userList } from './userList'

const app = new Koa()
import userRouter from './router/user'
import * as bodyParser from 'koa-bodyparser'

const APP_PORT = process.env.PORT || 3001

app.use(bodyParser())
app.use((ctx, next) => {
  console.log(ctx.request.method, ctx.request.url)
  next()
})

app.use((ctx, next) => {
  console.log(ctx.request.method, ctx.request.url)

  if (ctx.request.method === 'POST' && ctx.request.url === '/user/login') {
    next()
  }

  if (ctx.cookies.get('session_key')) {
    const cookieInfo = Buffer.from(ctx.cookies.get('session_key'), 'base64').toString()
    const sessionString: ISession = JSON.parse(cookieInfo)
    const newCookie = JSON.parse(cookieInfo)
    console.log(newCookie)

    const incrementCounter = newCookie.counter + 1
    newCookie.counter = incrementCounter
    ctx.cookies.set('session_key', Buffer.from(JSON.stringify(newCookie)).toString('base64'))
    const obj = userList.find((o) => o.id === sessionString.userId)
    if (obj.password === sessionString.userPassword) {
      if (!ctx.state.session) {
        // if (ctx.state.session.expires > Date.now()) {
        // }
        ctx.state.session = sessionString
      }
      next()
    }
  }
})

app.use(userRouter.routes())

app.listen(APP_PORT, () => {
  console.log('server is listening on port ', APP_PORT)
})
