import { Context } from 'koa'

import { ISession, User } from '../interfaces'
import { userList } from '../userList'

export function loginHandle(ctx: Context) {
  const user = ctx.request.body as unknown as User

  const existedUser = userList.find(({ id }) => id === user.id)
  if (existedUser && existedUser.password === user.password) {
    const session: ISession = {
      userId: existedUser.id,
      userPassword: existedUser.password,
      counter: 0,
      expires: Date.now() + 300000,
    }
    ctx.cookies.set('session_key', Buffer.from(JSON.stringify(session)).toString('base64'), { httpOnly: true })

    ctx.body = { registered: true }
  } else {
    ctx.body = { registered: false }
    ctx.status = 404
  }
}

export function getUsersHandler(ctx: Context) {
  // console.log(ctx.state)

  ctx.body = userList
}
