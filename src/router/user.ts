import * as Router from 'koa-router'
import { loginHandle, getUsersHandler } from '../controller/user'

const router = new Router({ prefix: '/user' })

router.get('/', getUsersHandler)

router.post('/login', loginHandle)

export default router
